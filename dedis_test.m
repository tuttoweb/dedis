function Y_hat = dedis_test(nn,X)
    nn=nnff(nn,X,X);
    Y_hat = nn.a{end};
end
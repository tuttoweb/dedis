function nn = dedis_train(X,Y,rbms,settings,X_val,Y_val)

    if (nargin<4)
        settings.use_val=false;
    end
    
    if (settings.use_val && ~(nargin>=6))
        error('You must specify X and Y for validation set (X_val, Y_val');
    end

    arch = zeros(1,numel(rbms)+1);
    M  = numel(rbms)/2;
    
    for i=1:numel(rbms)
        if (i<=M)
            arch(i) = rbms{i}.numberOfVisibleUnits;
        else
            arch(i:i+1) = [rbms{i}.numberOfHiddenUnits rbms{i}.numberOfVisibleUnits];
        end
    end
    
    nn = nnsetup(arch);
    nn.activation_function = 'sigm';
    nn.output = 'linear';
     
    nn.learningRate = 0.00001;
    
    % compute best minibatch size, discarding less images
    minibatch_sizes = 90:110;
    remainders = mod(size(X,1),minibatch_sizes);
    [rem,i] = min(remainders);
    
    X = X(1:end-rem,:);
    Y = Y(1:end-rem,:);   
   
    train_opts.batchsize = minibatch_sizes(i);
    train_opts.numepochs = 300;
    
    %plugging weights
    
    for i=1:numel(rbms)
        if (i<=M)
            nn.W{i} = cat(2,rbms{i}.b',rbms{i}.W');
        else
            nn.W{i} = cat(2,rbms{i}.c',rbms{i}.W);
        end
    end
    
    if (settings.use_val)
        nn = nntrain(nn, X, Y, train_opts, X_val, Y_val);
    else
        nn = nntrain(nn, X, Y, train_opts);
    end
    
    if (isfield(settings,'savepath'))
        save(settings.savepath,'nn');
    end
end

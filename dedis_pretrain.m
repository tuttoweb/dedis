function rbms = dedis_pretrain(X,Y,settings)
    if (~exist('settings','var') || ~isfield(settings,'verbose'))
        settings.verbose=0;
    end
    
    arch = [size(X,2),2250,1125,2250, size(Y,2) ];
    %arch must have a odd number of layers
    
    M = ceil(numel(arch)/2);
    
    rbms=cell(numel(arch)-1,1);
    
    %init machines
    for i=1:numel(rbms)
        if (i<M)
            if (i==1)
                rbm = GaussianBernoulliRestrictedBoltzmannMachine(arch(i),arch(i+1));
                gbrbm_special_code;
            else
                rbm = RestrictedBoltzmannMachine(arch(i),arch(i+1));
            end
        else
            if (i==numel(rbms))
                rbm = GaussianBernoulliRestrictedBoltzmannMachine(arch(i+1),arch(i));
                gbrbm_special_code;
             else
                rbm = RestrictedBoltzmannMachine(arch(i+1),arch(i));
            end
        end
        
        rbm.DataPreprocessing = DataPreprocessing.EmptyPreprocessor;
        rbm.TrainingEventListener.MaxIterations=200;
        
        if (settings.verbose)
            rbm.TrainingEventListener.Verbose=settings.verbose;
            rbm.Debug=1;
        end
            
        rbm.Eta.Value=0.00001;
        
        rbms{i} = rbm;
    end
    
    %start pretraining
    indexing = 1:numel(rbms);
    indexing(M:end) = indexing(end:-1:M);
    indexing = reshape(indexing,[],2);
    
    for i=1:2
        D = [];
        switch(i)
            case 1
                D = X;
            case 2
                D = Y;
        end
        
        for j=indexing(:,i)'
            rbms{j}.Train(D);
            D = rbms{j}.extractFeatures(D);
        end
        
        if (isfield(settings,'savepath'))
            save(settings.savepath,'rbms');
        end
    end
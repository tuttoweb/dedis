DEDIS
===================

Dependecies
--------

 1.  [Restricted Boltzmann Machine](https://bitbucket.org/teamrbm/rbm/src) library by Valerio Giuffrida
 2. The [DeepLearnToolbox](https://github.com/rasmusbergpalm/DeepLearnToolbox) by Rasmus Berg Palm

How it works
--------

Citation
--------

[1] V. Sevetlidis, **M. V. Giuffrida**, and S. A. Tsaftaris, “Whole Image Synthesis Using a Deep Encoder-Decoder Network,” in SASHIMI Workshop held in conjuction with MICCAI, Springer, 2016, pp. 127–137.